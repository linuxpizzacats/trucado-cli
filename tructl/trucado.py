import requests



class Trucado():
    "A simple class for trucado host api communication."
    
    def __init__(self,host,secure=False,port=8080):
        self.host=host
        self.port=port
        if secure:
            self.protocol="https://"
        else:
            self.protocol="http://"
    def post(self,path,body,headers={"Content-type": "application/json"}):
        return requests.post(self.protocol+self.host+":"+str(self.port)+"/"+path,data = body,
                              headers=headers)
    def get(self,path):
        return requests.get(self.protocol+self.host+":"+str(self.port)+"/"+path)
    
    def patch(self,path,body,headers={"Content-type": "application/json"}):
        return requests.patch(self.protocol+self.host+":"+str(self.port)+"/"+path,data = body,
                              headers=headers)
    def create_interface(self,name,highspeed):
        body='{ "name": "'+name+'","highspeed": "'+str(highspeed)+'" }'
        return self.post("interface",body)
    def get_interfaces(self):
        return self.get("interface")
    
    def get_interface(self,interface_id):
        return self.get("interface/"+str(interface_id))
        
    def set_interface_behaviour(self,interface_id,behaviour,aux=""):
        body='{ "behaviour": "'+behaviour+'", "aux": "'+aux+'" }'

        return self.patch("interface/"+str(interface_id),body)