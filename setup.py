#!/usr/bin/env python

from setuptools import find_packages
from setuptools import setup

PROJECT = 'tructl'

# Change docs/sphinx/conf.py too!
VERSION = '0.1'

try:
    long_description = open('README.rst', 'rt').read()
except IOError:
    long_description = ''

setup(
    name=PROJECT,
    version=VERSION,

    description='CLI app for trucado API',
    long_description=long_description,

    author='Julián Muhlberger',
    author_email='julian@comunidadtix.net.ar',

    url='https://gitlab.com/linuxpizzacats/trucado-cli',
    download_url='https://gitlab.com/linuxpizzacats/trucado-cli/-/archive/stable/trucado-cli-stable.tar.gz',

    classifiers=[
        'Development Status :: 1 - Planning',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3 :: Only',
        'Intended Audience :: Education',
        'Environment :: Console',
    ],

    platforms=['Any'],

    scripts=[],

    provides=[],
    install_requires=['cliff','simplejson','requests','logging','python-decouple'],

    namespace_packages=[],
    packages=find_packages(),
    include_package_data=True,

    entry_points={
        'console_scripts': [
            'tructl = tructl.main:main'
        ],
        'tructl.cli': [
            'config hostname = tructl.config:Hostname',
            'config port = tructl.config:Port',
            'create interface = tructl.interfaces:Create',            
            'get interfaces = tructl.interfaces:GetInterfaces',
            'get interface = tructl.interfaces:GetInterface',      
            'set admin status interface  = tructl.interfaces:SetAdminStatus',
            'set oper status interface  = tructl.interfaces:SetOperStatus',
            'set flapping interface  = tructl.interfaces:SetFlapping',
            'set usage interface  = tructl.interfaces:SetUsage',
            'set errors interface  = tructl.interfaces:SetErrors',
            'set discards interface  = tructl.interfaces:SetDiscards',
        ]
    },

    zip_safe=False,
)
