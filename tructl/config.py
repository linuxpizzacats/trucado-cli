import logging
from cliff.command import Command
from decouple import config

class Hostname(Command):
    """This command get or set the hostname to send commands"""

    log = logging.getLogger(__name__)
    
    def get_parser(self, prog_name):
        """Command argument parsing."""
        parser = super(Hostname, self).get_parser(prog_name)
        parser.add_argument("-s","--set", help="Set hostname to send request",
                    type=str, default="")

        return parser
    
    def take_action(self, parsed_args):
        if not parsed_args.set:
            host=config("TRUCTL_HOST",default="localhost")
            self.app.stdout.write('The selected hostname is: '+host+'\n')
        else:
            set_env_var("TRUCTL_HOST",parsed_args.set)

            
class Port(Command):
    """This command get or set the hostname's port to send commands"""

    log = logging.getLogger(__name__)
    
    def get_parser(self, prog_name):
        """Command argument parsing."""
        parser = super(Port, self).get_parser(prog_name)
        parser.add_argument("-s","--set", help="Set hostname's port to send request",
                    type=int)

        return parser
    
    def take_action(self, parsed_args):
        if not parsed_args.set:
            port=config("TRUCTL_HOST_PORT",default=8080,cast=int)
            self.app.stdout.write('The selected port is: '+str(port)+'\n')
        else:
            set_env_var("TRUCTL_HOST_PORT",parsed_args.set)
            
def set_env_var(name,value):
    with open(".env","r") as file:
        lines = file.readlines()
    filew= open(".env","w")
    for line in lines:
        if line.strip().split("=")[0]==name:
            line=name+"="+str(value)+"\n"
        filew.write(line)
    filew.close()
    
    
            