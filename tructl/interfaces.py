import logging
from cliff.lister import Lister
from cliff.show import ShowOne
from cliff.command import Command
from tructl.trucado import Trucado
import json

class GetInterfaces(Lister):
    """Show a list of the interfaces for the current selected trucado API server."""

    log = logging.getLogger(__name__)
    def take_action(self, parsed_args):
        result= Trucado("localhost").get_interfaces().text
        columns=('Id','Name','Admin','Oper','Description')
        return (columns,
                ((interfaz["id"], interfaz["name"]["value"],"up" if interfaz["adminStatus"]["value"]=="1" else "down","up" if interfaz["operStatus"]["value"]=="1" else "down",interfaz["alias"]["value"]) for interfaz in json.loads(result))
                )

class GetInterface(ShowOne):
    """Show an Interface by id"""

    log = logging.getLogger(__name__)
    def get_parser(self, prog_name):
        """Command argument parsing."""
        parser = super(GetInterface, self).get_parser(prog_name)
        parser.add_argument("id", help="Id of the desired interface to be displayed.",
                    type=int)

        return parser    
    
    def take_action(self, parsed_args):
        result= Trucado("localhost").get_interface(parsed_args.id).text
        columns=('Id','Index','Name','Admin / Oper Status','Description','Highspeed','In / Out','In / Out discards','In / Out errors')
        result=json.loads(result)
        return columns, (result["id"],result["index"]["value"],
                         result["name"]["value"],str("up" if result["adminStatus"]["value"]=="1" else "down")+" / "+str("up" if result["operStatus"]["value"]=="1" else "down"),
                         result["alias"]["value"],result["highspeed"]["value"],
                         result["hcInOctects"]["value"]+" bps / "+result["hcOutOctects"]["value"]+" bps",
                         result["inDiscards"]["value"]+" pps / "+result["outDiscards"]["value"]+" pps",
                         result["inErrors"]["value"]+" pps / "+result["outErrors"]["value"]+" pps")
class Create(Command):
    """This command sends POST to trucado with a new InterfaceDTO json"""

    log = logging.getLogger(__name__)
    
    def get_parser(self, prog_name):
        """Command argument parsing."""
        parser = super(Create, self).get_parser(prog_name)
        parser.add_argument("name", help="Desired name for the new interface.",
                    type=str)
        parser.add_argument("-t","--type", help="Desired type for the desired interface. It defines Highspeed.",
                    type=str, choices=["Ethernet","FastEthernet","GigaEthernet","TenGigaEthernet","HundredGigaEthernet"],default="GigaEthernet")

        return parser
    
    def take_action(self, parsed_args):
        highspeed=self.resolve_highspeed(parsed_args.type)
        result= Trucado("localhost").create_interface(parsed_args.name,highspeed).status_code
        if result==201:
            self.app.stdout.write('Interface created.\n')
        else:
            self.app.stdout.write('An error ocurred during the new interface creation.\n')
        
    def resolve_highspeed(self,interface_type):
        """Getting highspeed for matching interface_type. If not, an exception is raised"""
        if interface_type=="Ethernet":
            return 10
        elif interface_type=="FastEthernet" :
            return 100
        elif interface_type=="GigaEthernet" :
            return 1000
        elif interface_type=="TenGigaEthernet" :
            return 10000      
        elif interface_type=="TenGigaEthernet" :
            return 10000  
        elif interface_type=="HundredGigaEthernet" :
            return 100000
        else:
            self.log.info("Interface's interface_type not found.")
            raise RuntimeError("Could not resolve highspeed for the given interface's interface_type.") 
                
class SetAdminStatus(Command):
    """This command send PATCH to trucado with AdminDown or AdminUp behaviour"""

    log = logging.getLogger(__name__)
    
    def get_parser(self, prog_name):
        """Command argument parsing."""
        parser = super(SetAdminStatus, self).get_parser(prog_name)
        parser.add_argument("id", help="Id of the desired interface to be set with the new admin status.",
                    type=int)
        parser.add_argument("status", help="Desired admin status.",
                    type=str, choices=["up","down"])

        return parser
    
    def take_action(self, parsed_args):
        behaviour=self.resolve_status(parsed_args.status)
        result= Trucado("localhost").set_interface_behaviour(parsed_args.id,behaviour).status_code
        if result==204:
            self.app.stdout.write('Interface set as admin '+parsed_args.status+'.s\n')
        else:
            self.app.stdout.write('An error ocurred during setting interface\'s admin status.\n')
        
    def resolve_status(self,desired_status):    
        if desired_status=="up":
            return "org.lpc.models.commands.interfaces.AdminUp"
        else:
            return "org.lpc.models.commands.interfaces.AdminDown"
        
class SetOperStatus(Command):
    """This command send PATCH to trucado with OperDown or OperUp behaviour"""

    log = logging.getLogger(__name__)
    
    def get_parser(self, prog_name):
        """Command argument parsing."""
        parser = super(SetOperStatus, self).get_parser(prog_name)
        parser.add_argument("id", help="Id of the desired interface to be set with the new oper status.",
                    type=int)
        parser.add_argument("status", help="Desired oper status.",
                    type=str, choices=["up","down"])

        return parser
    
    def take_action(self, parsed_args):
        behaviour=self.resolve_status(parsed_args.status)
        result= Trucado("localhost").set_interface_behaviour(parsed_args.id,behaviour).status_code
        if result==204:
            self.app.stdout.write('Interface set as operational '+parsed_args.status+'.\n')
        else:
            self.app.stdout.write('An error ocurred during setting interface\'s operational status.\n')
        
    def resolve_status(self,desired_status):    
        if desired_status=="up":
            return "org.lpc.models.commands.interfaces.OperUp"
        else:
            return "org.lpc.models.commands.interfaces.OperDown"     
           
class SetFlapping(Command):
    """This command send PATCH to trucado with Flapping behaviour"""

    log = logging.getLogger(__name__)
    
    def get_parser(self, prog_name):
        """Command argument parsing."""
        parser = super(SetFlapping, self).get_parser(prog_name)
        parser.add_argument("id", help="Id of the desired interface to be set be in flapping status.",
                    type=int)

        return parser
    
    def take_action(self, parsed_args):
        behaviour="org.lpc.models.commands.interfaces.Flapping"
        result= Trucado("localhost").set_interface_behaviour(parsed_args.id,behaviour).status_code
        if result==204:
            self.app.stdout.write('Interface set as flapping '+parsed_args.status+'.\n')
        else:
            self.app.stdout.write('An error ocurred applying the desired behaviour.\n')

class SetUsage(Command):
    """This command send PATCH to trucado with UsageAt behaviour for boths, in, out"""

    log = logging.getLogger(__name__)
    
    def get_parser(self, prog_name):
        """Command argument parsing."""
        parser = super(SetUsage, self).get_parser(prog_name)
        parser.add_argument("id", help="Id of the desired interface to be set with the new usage percent.",
                    type=int)
        parser.add_argument("-d","--direction", help="Desired direction for traffic usage.",
                    type=str, choices=["both","in","out"],default="both")
        parser.add_argument("-p","--percentage", help="Desired percentant interface's usage.",
                    type=int,default=0)        

        return parser
    
    def take_action(self, parsed_args):
        behaviour=self.resolve_direction(parsed_args.direction)
        result= Trucado("localhost").set_interface_behaviour(parsed_args.id,behaviour,str(parsed_args.percentage)).status_code
        if result==204:
            self.app.stdout.write('Interface usage set to '+str(parsed_args.percentage)+'% for "'+parsed_args.direction+'" direction.\n')
        else:
            self.app.stdout.write('An error ocurred during setting interface\'s usage.\n')
        
    def resolve_direction(self,direction):    
        if direction=="both":
            return "org.lpc.models.commands.interfaces.UsageAt"
        elif direction=="in":
            return "org.lpc.models.commands.interfaces.UsageInAt"   
        elif direction=="out":
            return "org.lpc.models.commands.interfaces.UsageOutAt"
        
class SetErrors(Command):
    """This command send PATCH to trucado with WithErrors behaviour for boths, in, out"""

    log = logging.getLogger(__name__)
    
    def get_parser(self, prog_name):
        """Command argument parsing."""
        parser = super(SetErrors, self).get_parser(prog_name)
        parser.add_argument("id", help="Id of the desired interface to be set with errors",
                    type=int)
        parser.add_argument("-d","--direction", help="Desired direction for errors.",
                    type=str, choices=["both","in","out"],default="both")        

        return parser
    
    def take_action(self, parsed_args):
        behaviour=self.resolve_direction(parsed_args.direction)
        result= Trucado("localhost").set_interface_behaviour(parsed_args.id,behaviour).status_code
        if result==204:
            self.app.stdout.write('Interface is set to generate errors for "'+parsed_args.direction+'" direction.\n')
        else:
            self.app.stdout.write('An error ocurred during setting interface with errors.\n')
        
    def resolve_direction(self,direction):    
        if direction=="both":
            return "org.lpc.models.commands.interfaces.WithErrors"
        elif direction=="in":
            return "org.lpc.models.commands.interfaces.WithErrorsIn"   
        elif direction=="out":
            return "org.lpc.models.commands.interfaces.WithErrorsOut"
        
class SetDiscards(Command):
    """This command send PATCH to trucado with WithDiscards behaviour for both, in, out"""

    log = logging.getLogger(__name__)
    
    def get_parser(self, prog_name):
        """Command argument parsing."""
        parser = super(SetDiscards, self).get_parser(prog_name)
        parser.add_argument("id", help="Id of the desired interface to be set with discards",
                    type=int)
        parser.add_argument("-d","--direction", help="Desired direction for discards.",
                    type=str, choices=["both","in","out"],default="both")        

        return parser
    
    def take_action(self, parsed_args):
        behaviour=self.resolve_direction(parsed_args.direction)
        result= Trucado("localhost").set_interface_behaviour(parsed_args.id,behaviour).status_code
        if result==204:
            self.app.stdout.write('Interface is set to generate discards for "'+parsed_args.direction+'" direction.\n')
        else:
            self.app.stdout.write('An error ocurred during setting interface with discards.\n')
        
    def resolve_direction(self,direction):    
        if direction=="both":
            return "org.lpc.models.commands.interfaces.WithDiscards"
        elif direction=="in":
            return "org.lpc.models.commands.interfaces.WithDiscardsIn"   
        elif direction=="out":
            return "org.lpc.models.commands.interfaces.WithDiscardsOut"                